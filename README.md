# Domoticz Blind Control Script

This Lua script is designed to automate the control of blinds in a Domoticz home automation system based on specific time conditions. The script leverages various user-defined variables to configure different aspects of blind control, such as sunrise and sunset offsets, opening and closing hours, and the management of blinds during the summer.

## Purpose

The primary purpose of this script is to intelligently manage the opening and closing of blinds throughout the day based on user-defined parameters. The script considers factors like sunrise, sunset, and specific time ranges to determine when to open or close blinds. Additionally, it provides flexibility for different configurations during the summer months.

## Configuration

### User Variables

ALl variables must be created.

1. **Summer Configuration**
   * `automaticSummerDetection`: Set to 1 to enable summer detection with openwheather. Openwheather is queried to get the max temperature Expected in the current day.
   * `summerThreshold`: The threshold temperature to detects if summer mode. 
   * `owApiURL`: The openwheather api url to query openwheather. Must be https://api.openweathermap.org/data/2.5/forecast but can change if openwheather update the api.
   * `owApiKey`: The openwheather api key to query openwheather. 
   * `owCity`: the City to query
   * `summer`: Set to `1` to force summer mode.
2. **Blinds Configuration**

   * `blinds`: Comma-separated list of all blinds to be managed
   * `blindsToKeepClosedInSunrise`: Comma-separated list of blinds to keep closed during sunrise. Must be a sublist of blinds
   * `blindsWithMorningSun`: Comma-separated list of blinds exposed to morning sunlight (if summer is enabled). Must be a sublist of blinds
   * `blindsWithAfternoonSun`: Comma-separated list of blinds exposed to afternoon sunlight (if summer is enabled). Must be a sublist of blinds
3. **Time Offsets**

   * `sunriseOffset`: Offset in minutes for sunrise. The blinds will be opened `sunriseOffset` minutes after sunrise on non-weekend days.
   * `weekendSunriseOffset`: Offset in minutes for sunrise on weekends. The blinds will be opened `weekendSunriseOffset` minutes after sunrise on weekend days.
   * `sunsetOffset`: Offset in minutes for sunset. The blinds will be closed `sunsetOffset` minutes after senset.
4. **Specific Hours Configuration**

   * `midMorningOpenHour`: Hour at which blinds open in mid-morning. This will open the other blinds not listed in blindsToKeepClosedInSunrise and in blindsWithMorningSun.
   * `midMorningOpenHour`: Hour at which blinds open in mid-morning on weekend. This will open the other blinds not listed in blindsToKeepClosedInSunrise and in blindsWithMorningSun.
   * `middayOpenHour`: Hour at which blinds open in midday (if summer is enabled).
   * `afternoonCloseHour`: Hour at which blinds close in the afternoon (if summer is enabled).
   * `openEveningHour`: Hour at which blinds open in the evening (if summer is enabled).
5. **Summer-Specific Configuration**

### Flags

Flags are used to track the workflow of each day. These flags should be created as user variables in Domoticz.

* `flagOpenSunrise`: Flag to track if blinds were opened during sunrise.
* `flagOpenMidMorning`: Flag to track if blinds were opened in mid-morning.
* `flagOpenMidday`: Flag to track if blinds were opened in midday.
* `flagCloseAfternoon`: Flag to track if blinds were closed in the afternoon.
* `flagOpenEvening`: Flag to track if blinds were opened in the evening.
* `flagCloseSunset`: Flag to track if blinds were closed at sunset.

### Running the Script

Integrate this script into Domoticz by setting it up as a Time event. Ensure that the necessary user variables and flags are defined in Domoticz.

## Example Configuration

Here is an example configuration:

<pre><code class="!whitespace-pre hljs language-lua">
automaticSummerDetection = 1
summerThreshold = 30
owApiKey = XXX
owCity = Angers,FR
owApiURL = https://api.openweathermap.org/data/2.5/forecast
summer = 0
owDailyMaxTemperature = 0
blinds = "Living Room,Kitchen,Bedroom"
blindsToKeepClosedInSunrise = "Bedroom"
sunriseOffset = 10
weekendSunriseOffset = 15
sunsetOffset = -15
midMorningOpenHour = "07:00"
middayOpenHour = "12:00"
afternoonCloseHour = "15:30"
openEveningHour = "18:00"
blindsWithMorningSun = "Kitchen,Bedroom"
blindsWithAfternoonSun = "Living Room,Office"

flagOpenSunrise = 0
flagOpenMidMorning = 0
flagOpenMidday = 0
flagCloseAfternoon = 0
flagOpenEvening = 0
flagCloseSunset = 0
flagTodayMaxTempCalculated = 0
</code></pre>

Adjust the configuration based on your specific needs and preferences.
