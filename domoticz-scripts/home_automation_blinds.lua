-- Function to check if the current day is a weekend (Saturday or Sunday)
local function isWeekend()
    local dayNow = tonumber(os.date("%w"))
    return dayNow == 0 or dayNow == 6
end

-- Function to check if the current time is within a specified time range
local function isInTimeRange(startTime, endTime)
    local time = os.date("%H:%M")
    return time >= startTime and time < endTime
end

-- Function to manage blinds based on the provided command (Open or Close)
local function doActionOnDevices(devices, command, commandArray)
    for _, device in ipairs(devices) do
        local currentStatus = otherdevices[device]
        if currentStatus ~= command then
            commandArray[device] = command
        end
    end
end

-- Function to open specified blinds
local function openBlinds(blinds, commandArray)
    doActionOnDevices(blinds, 'Open', commandArray)
end

-- Function to close specified blinds
local function closeBlinds(blinds, commandArray)
    doActionOnDevices(blinds, 'Close', commandArray)
end

-- Function to split a string based on a delimiter
local function split(inputString, delimiter)
    local result = {}
    for match in (inputString .. delimiter):gmatch("(.-)" .. delimiter) do
        table.insert(result, match)
    end
    return result
end

-- Function to reduce array1 by removing common elements in array2
function reduceStringArray(array1, array2)
    local result = {}
    local setArray2 = {}
    for _, value in ipairs(array2) do
        setArray2[value] = true
    end
    for _, value in ipairs(array1) do
        if not setArray2[value] then
            table.insert(result, value)
        end
    end
    return result
end

-- Function to convert minutes to hours and minutes
local function convertMinutesToHoursAndMinutes(minutes)
    local hours = math.floor(minutes / 60)
    local remainingMinutes = minutes % 60
    return string.format("%02d:%02d", hours, remainingMinutes)
end

-- Function to make an HTTP request
function http_request(url)
    local command = string.format('curl -s "%s"', url)
    local handle = io.popen(command)
    local result = handle:read("*all")
    handle:close()
    return result
end

-- Function to get the maximum temperature from the forecast
function getMaxTemperature(baseUrl, city, api_key)
    local success, response = pcall(http_request, baseUrl .. "?q=" .. city .. "&APPID=" .. api_key .. "&units=metric&cnt=10")
    local maxTemp = 0
    if (success) then
        -- TODO : find a way to get this without hardcoding...
        local json = (loadfile "/home/vincent.lelostec/domoticz/scripts/lua/JSON.lua")()
        local data = json:decode(response)
        if (data and data.list) then
            for _, forecast in ipairs(data.list) do
                local main = forecast.main
                if main and main.temp_max then
                    maxTemp = math.max(maxTemp, main.temp_max)
                end
            end
        end
    else
        print("Error fetching data from the weather API:", response)
    end
    return maxTemp
end

-- All configured parameters
local config = {
    summer = tonumber(uservariables["summer"]) or 0, -- manually activate summer mode
    automaticSummerDetection = tonumber(uservariables["automaticSummerDetection"]) or 0,
    summerThreshold = tonumber(uservariables["summerThreshold"]) or 0, -- automatic summer detection with openweather API
    blinds = split(uservariables["blinds"], ","), -- TODO : get blinds with devices by type but can't find this in lua context
    blindsToKeepClosedInSunrise = split(uservariables["blindsToKeepClosedInSunrise"], ","),
    sunriseOffset = tonumber(uservariables["sunriseOffset"]) or 0,
    weekendSunriseOffset = tonumber(uservariables["weekendSunriseOffset"]) or 0,
    midMorningOpenHour = uservariables["midMorningOpenHour"],
    midMorningOpenHourWeekend = uservariables["midMorningOpenHourWeekend"],
    sunsetOffset = tonumber(uservariables["sunsetOffset"]) or 0,
    -- summer conf
    blindsWithMorningSun = split(uservariables["blindsWithMorningSun"], ","),
    blindsWithAfternoonSun = split(uservariables["blindsWithAfternoonSun"], ","),
    middayOpenHour = uservariables["middayOpenHour"],
    afternoonCloseHour = uservariables["afternoonCloseHour"],
    openEveningHour = uservariables["openEveningHour"],
    -- openweather configuration
    owApiKey = uservariables["owApiKey"],
    owCity = uservariables["owCity"],
    owApiURL = uservariables["owApiURL"],
}

-- All flag to follow the workflow of each day
local flags = {
    flagOpenSunrise = tonumber(uservariables["flagOpenSunrise"]) or 0,
    flagOpenMidMorning = tonumber(uservariables["flagOpenMidMorning"]) or 0,
    flagOpenMidday = tonumber(uservariables["flagOpenMidday"]) or 0,
    flagCloseAfternoon = tonumber(uservariables["flagCloseAfternoon"]) or 0,
    flagOpenEvening = tonumber(uservariables["flagOpenEvening"]) or 0,
    flagCloseSunset = tonumber(uservariables["flagCloseSunset"]) or 0,
    flagTodayMaxTempCalculated = tonumber(uservariables["flagTodayMaxTempCalculated"]) or 0,
    owDailyMaxTemperature = tonumber(uservariables["owDailyMaxTemperature"]) or 0
}

local function setFlags(flagOpenSunrise, flagOpenMidMorning, flagOpenMidday, flagCloseAfternoon, flagOpenEvening, flagCloseSunset)
    flags.flagOpenSunrise = flagOpenSunrise
    flags.flagOpenMidMorning = flagOpenMidMorning
    flags.flagOpenMidday = flagOpenMidday
    flags.flagCloseAfternoon = flagCloseAfternoon
    flags.flagOpenEvening = flagOpenEvening
    flags.flagCloseSunset = flagCloseSunset
end

-- Function to update user-defined flags in Domoticz
local function updateFlags(commandArray)
    for key, value in pairs(flags) do
        local currentVariableValue = uservariables[key]
        if currentVariableValue ~= value then
            commandArray["Variable:" .. key] = tostring(value)
        end
    end
end

-- Function to check if a string is not blank (non-null and non-empty)
local function isNotBlank(str)
    return str and str ~= ""
end

-- Checks if config is valid to active automatic summer detection
local function isValidSummerDetectionConfig()
    local valid = true
    if (config.automaticSummerDetection == 1) then
        if (not isNotBlank(config.owApiKey)) then
            print("Automatic summer detection disabled, `owApiKey` should be configured.")
            valid = false
        end
        if (not isNotBlank(config.owCity)) then
            print("Automatic summer detection disabled, `owCity` should be configured.")
            valid = false
        end
        if (not isNotBlank(config.owApiURL)) then
            print("Automatic summer detection disabled, `owApiURL` should be configured.")
            valid = false
        end
        if (config.summerThreshold <= 0) then
            print("Automatic summer detection disabled, `summerThreshold` should be configured.")
            valid = false
        end
    end
    return valid
end

-- Command array to store Domoticz commands
commandArray = {}

-- Gets the hour for opening blinds on sunrise
local sunriseTime = timeofday["SunriseInMinutes"]
local morningOpenHour = convertMinutesToHoursAndMinutes(sunriseTime + config.sunriseOffset)
if isWeekend() then
    morningOpenHour = convertMinutesToHoursAndMinutes(sunriseTime + config.weekendSunriseOffset)
end

-- Gets the hour for opening blinds on mid morning, depending on day
local midMorningHour = config.midMorningOpenHour
if isWeekend() then
    midMorningHour = config.midMorningOpenHourWeekend
end

-- Secure the day workflow, morning open hour must be lesser than midMorningHour
if morningOpenHour >= midMorningHour then
    morningOpenHour = convertMinutesToHoursAndMinutes(morningOpenHour-10)
end

    -- Gets the hour for closing blinds on sunset
local sunsetCloseHour = convertMinutesToHoursAndMinutes(timeofday["SunsetInMinutes"] + config.sunsetOffset)

-- If automatic summer detection is enabled, try to get the max temp for this day if not set
-- The calculation is done only after starting a new day
summer = config.summer == 1
if (not isInTimeRange(sunsetCloseHour, "23:59") and config.automaticSummerDetection == 1) then
    if (flags.flagTodayMaxTempCalculated == 0 and isValidSummerDetectionConfig()) then
        flags.owDailyMaxTemperature = getMaxTemperature(config.owApiURL, config.owCity, config.owApiKey)
        flags.flagTodayMaxTempCalculated = 1
    end
    if (flags.owDailyMaxTemperature > 0) then
        summer = flags.owDailyMaxTemperature >= config.summerThreshold
    end
end

-- Logic to control blinds based on time and conditions
if isInTimeRange(morningOpenHour, midMorningHour) and flags.flagOpenSunrise == 0 then
    -- Open all blinds on sunrise, except blinds in blindsToKeepClosedInSunrise
    toOpen = reduceStringArray(config.blinds, config.blindsToKeepClosedInSunrise)
    if (summer) then
      toOpen = reduceStringArray(toOpen, config.blindsWithMorningSun)
    end
    openBlinds(toOpen, commandArray)
    setFlags(1, 0, 0, 0, 0, 0)
elseif isInTimeRange(midMorningHour, config.middayOpenHour) and flags.flagOpenMidMorning == 0 then
    -- Open all blinds in blindsToKeepClosedInSunrise at the hour configured for mid morning
    -- If summer is enabled, opens only blinds in blindsToKeepClosedInSunrise and not in blindsWithMorningSun
    if (summer) then
        openBlinds(reduceStringArray(config.blindsToKeepClosedInSunrise, config.blindsWithMorningSun), commandArray)
    else
        openBlinds(config.blindsToKeepClosedInSunrise, commandArray)
    end
    setFlags(1, 1, 0, 0, 0, 0)
elseif isInTimeRange(config.middayOpenHour, config.afternoonCloseHour) and flags.flagOpenMidday == 0 then
    -- On summer, open blinds configured on blindsWithMorningSun at the hour configured for midday
    if (summer) then
        openBlinds(config.blindsWithMorningSun, commandArray)
    end
    setFlags(1, 1, 1, 0, 0, 0)
elseif isInTimeRange(config.afternoonCloseHour, config.openEveningHour) and flags.flagCloseAfternoon == 0 then
    -- On summer, close blinds configured on blindsWithAfternoonSun at the hour configured for afternoon
    if (summer) then
        closeBlinds(config.blindsWithAfternoonSun, commandArray)
    end
    setFlags(1, 1, 1, 1, 0, 0)
elseif isInTimeRange(config.openEveningHour, sunsetCloseHour) and flags.flagOpenEvening == 0 then
    -- On summer, open blinds configured on blindsWithAfternoonSun at the hour configured for evening
    if (summer) then
        openBlinds(config.blindsWithAfternoonSun, commandArray)
    end
    setFlags(1, 1, 1, 1, 1, 0)
elseif isInTimeRange(sunsetCloseHour, "23:59") and flags.flagCloseSunset == 0 then
    -- Close all blinds at sunsetCloseHour
    closeBlinds(config.blinds, commandArray)
    setFlags(0, 1, 1, 1, 1, 1)
    -- Reset the max temp calculation flag
    flags.flagTodayMaxTempCalculated = 0
    flags.owDailyMaxTemperature = 0
end

-- Fill the update flags commands
updateFlags(commandArray)

return commandArray
